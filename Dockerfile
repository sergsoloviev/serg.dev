# build stage
FROM golang:alpine AS build-env
ADD . /go/src/bitbucket.org/sergsoloviev/serg.dev
RUN apk update; apk add --no-cache gcc libc-dev
RUN cd /go/src/bitbucket.org/sergsoloviev/serg.dev && go test -v ./... && go build -v -o app

# final stage
FROM alpine
WORKDIR /app
COPY --from=build-env /go/src/bitbucket.org/sergsoloviev/serg.dev/app /app/
ENTRYPOINT ./app