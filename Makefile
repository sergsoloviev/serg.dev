all: test build

test:
	go test -v ./...

build:
	docker build --force-rm -t serg.dev:latest .

run:
	docker run -p8080:8080 --rm --name serg.dev-instance serg.dev

.PHONY: test build