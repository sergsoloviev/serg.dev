package views

import (
	"net/http"
	"net/http/httptest"
	"testing"
)

func TestIndex(t *testing.T) {
	request, err := http.NewRequest("GET", "/", nil)
	if err != nil {
		t.Fatal(err)
	}
	w := httptest.NewRecorder()
	handler := http.HandlerFunc(Index)
	handler.ServeHTTP(w, request)

	response := w.Result()
	if response.StatusCode != http.StatusOK {
		t.Errorf("Unexpected status code %d", response.StatusCode)
	}
}
