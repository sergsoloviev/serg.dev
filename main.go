package main

import (
	"fmt"
	"log"
	"net/http"

	"bitbucket.org/sergsoloviev/serg.dev/views"
)

var ()

func main() {

	http.HandleFunc("/", views.Index)
	listen := fmt.Sprintf("%s:%d", "0.0.0.0", 8080)
	log.Println("http://" + listen)
	err := http.ListenAndServe(listen, nil)
	if err != nil {
		panic(err)
	}
}
